import Data from "../Data";
import AdsManager from "../AdsManager";
import GameManager from "../GameManager";

const {ccclass, property} = cc._decorator;
const tween = cc.tween

const EFFECT_DURATION = .5
const EFFECT_DISTANCE = 793

const CATE_NAME = ['Tuong', 'Sannha', 'Giuong', 'Tivi', 'cuaso', 'Anh', 'Boncau']
const TYPE_NAME = ['DEFAULT', 'TYPE1', 'TYPE2', 'TYPE3', 'TYPE4', 'NONE']
const UNLOCK_LEVEL = [
    [0, 1, 6, 14, 20],
    [0, 1, 8, 14, 22],
    [2, 2, 8, 18, 22, 0],
    [0, 2, 10, 16, 24],
    [0, 4, 10, 18, 24],
    [0, 4, 12, 18, 26],
    [0, 6, 12, 20, 26],
]
const PRICE = [
    [0, 100, -1, 400, 600],
    [0, 100, -1, 400, 600],
    [0, 150, -1, 400, 600, 0],
    [0, 150, -1, 400, 600],
    [0, 200, -1, 400, 600],
    [0, 200, -1, 400, 600],
    [0, 200, -1, 400, 600],
]
const DEFAULT_CURRENT = {
    Tuong: {
        cateIndex: 0,
        type: 'DEFAULT',
    },
    Sannha: {
        cateIndex: 0,
        type: 'DEFAULT',
    },
    Giuong: {
        cateIndex: 0,
        type: 'NONE',
    },
    Tivi: {
        cateIndex: 0,
        type: 'DEFAULT',
    },
    cuaso: {
        cateIndex: 0,
        type: 'DEFAULT',
    },
    Anh: {
        cateIndex: 0,
        type: 'DEFAULT',
    },
    Boncau: {
        cateIndex: 0,
        type: 'DEFAULT',
    },
}

@ccclass
export default class Shop extends cc.Component {

    @property(cc.Node)
    background: cc.Node = null

    @property([cc.Node])
    itemContents: cc.Node[] = []

    @property(cc.Node)
    overlay: cc.Node = null

    @property(cc.Node)
    back: cc.Node = null

    @property(cc.Node)
    adsMoney: cc.Node = null

    @property(cc.Node)
    confirm: cc.Node = null

    private _gameManager = null
    private _uiController = null
    private _homeController = null

    private _itemNodes = []

    private _confirmDeco = {cateIndex: 0, typeIndex: 0}

    onLoad() {
        this._gameManager = cc.find("Canvas/GameManager").getComponent("GameManager")
        this._uiController = cc.find("Canvas/UI").getComponent("UIController")

        const home = cc.find("Canvas/Home")
        home && (this._homeController = home.getComponent("HomeController"))

        this.overlay.active = false
        this.back.active = false
        this.adsMoney.active = false
        this.confirm.active = false
    }

    onEnable() {
        this.background.opacity = 0;
        this.background.position = cc.v3(0, -1377)

        this.effectOpen()
        this.setUnlockItems()
    }

    public static serilizeUnlockedDeco(gameData) {
        if (gameData.unlockedDeco instanceof Array) {
            const unlockedDeco = {}

            gameData.unlockedDeco.forEach((v, i) => {
                unlockedDeco[CATE_NAME[i]] = {}
                v.forEach((v2, i2) => {
                    unlockedDeco[CATE_NAME[i]][TYPE_NAME[i2]] = v2
                })
            })

            gameData.unlockedDeco = unlockedDeco
            return unlockedDeco
        }

        return gameData.unlockedDeco
    }

    getUnlockedDeco(gameData) {
        const unlockedDeco = Shop.serilizeUnlockedDeco(gameData)
        return unlockedDeco
    }

    setUnlockItems(cb = null) {
        Data.getData(Data.FACEBOOK_KEY, (err, gameData) => {
            const money = gameData.money
            const unlockedLevel = gameData.unlockedLevel || 0
            const unlockedDeco = this.getUnlockedDeco(gameData) || {}
            const currentDeco = {
                ...DEFAULT_CURRENT,
                ...gameData.currentDeco
            }

            UNLOCK_LEVEL.forEach((lvlCate, index) => {
                const cate = CATE_NAME[index]

                !unlockedDeco[cate] && (unlockedDeco[cate] = {})

                if (!this._itemNodes[index]) {
                    this._itemNodes[index] = []
                }

                lvlCate.forEach((lvl, index2) => {
                    const type = TYPE_NAME[index2]
                    const item = this.itemContents[index].getChildByName(`Item${index2 || ''}`)
                    const isCurrent = currentDeco[cate] && currentDeco[cate].type === type ? true : false

                    !unlockedDeco[cate][type] && (unlockedDeco[cate][type] = false)

                    if (!this._itemNodes[index][index2]) {
                        this._itemNodes[index][index2] = {
                            'tick': item.getChildByName('Tick'),
                            'require': item.getChildByName('Require'),
                            'payment': item.getChildByName('Payment'),
                            'ads': item.getChildByName('Ads'),
                        }
                    }

                    const price = PRICE[index][index2]
                    const levelNode = this._itemNodes[index][index2].require.getChildByName('Level')
                    const priceNode = this._itemNodes[index][index2].payment.getChildByName('Price')
                    const priceColor = money < price ? [255, 32, 0] : [255, 255, 255]

                    levelNode.getComponent(cc.Label).string = `LEVEL ${lvl + 1}`

                    priceNode.getComponent(cc.Label).string = price
                    priceNode.color = new cc.Color(...priceColor)

                    if (!lvl) {
                        unlockedDeco[cate][type] = true

                        this._itemNodes[index][index2].tick.active = isCurrent
                        this._itemNodes[index][index2].require.active = false
                        this._itemNodes[index][index2].payment.active = false
                        this._itemNodes[index][index2].ads.active = false

                    } else if (unlockedLevel > lvl) {
                        if (price === 0) {
                            unlockedDeco[cate][type] = true

                            this._itemNodes[index][index2].tick.active = isCurrent
                            this._itemNodes[index][index2].require.active = false
                            this._itemNodes[index][index2].payment.active = false
                            this._itemNodes[index][index2].ads.active = false
                        }
                        else if (unlockedDeco[cate][type]) {
                            this._itemNodes[index][index2].tick.active = isCurrent
                            this._itemNodes[index][index2].require.active = false
                            this._itemNodes[index][index2].payment.active = false
                            this._itemNodes[index][index2].ads.active = false

                        } else {
                            this._itemNodes[index][index2].tick.active = false
                            this._itemNodes[index][index2].require.active = false
                            this._itemNodes[index][index2].payment.active = price > 0
                            this._itemNodes[index][index2].ads.active = price === -1
                        }

                    } else {
                        this._itemNodes[index][index2].tick.active = false
                        this._itemNodes[index][index2].require.active = true
                        this._itemNodes[index][index2].payment.active = false
                        this._itemNodes[index][index2].ads.active = false
                    }
                })
            })

            gameData.unlockedDeco = unlockedDeco
            Data.saveData(gameData, Data.FACEBOOK_KEY)
            cb && cb(unlockedDeco)
        })
    }

    effectOpen() {
        this._uiController.showUIControlling(false)
        this._homeController.moveVertical(-360, EFFECT_DURATION)

        tween(this.background)
            .by(EFFECT_DURATION, {position: cc.v3(0, EFFECT_DISTANCE), opacity: 255})
            .start()
    }

    effectClose(cb = () => {}) {
        this._uiController.showUIControlling(true)
        this._homeController.moveVertical(360, EFFECT_DURATION)

        tween(this.background)
            .by(EFFECT_DURATION, {position: cc.v3(0, -EFFECT_DISTANCE), opacity: -255})
            .call(cb)
            .start()
    }

    onClose(e) {
        tween(e.target)
            .by(.05, {scale: -.1})
            .by(.05, {scale: .1})
            .call(() => {
                this.effectClose(() => {
                    this.node.active = false
                })
            })
            .start()
    }

    onSelectCate(e, customData) {
        const itemIndex = parseInt(customData)
        const cateContent = e.target.parent

        cateContent.children.forEach((node) => {
            const selectedNode = node.getChildByName('Selected')
            if (node === e.target) {
                selectedNode.active = true
            } else {
                selectedNode.active = false
            }
        });

        this.itemContents.forEach((node, index) => {
            if (index === itemIndex) {
                node.active = true
            } else {
                node.active = false
            }
        })
    }

    onSelectItem(e, customData) {
        tween(e.target).by(.1, {scale: .1}).by(.1, {scale: -.1}).start()

        Data.getData(Data.FACEBOOK_KEY, (err, gameData) => {
            const money = gameData.money
            const unlockedLevel = gameData.unlockedLevel
            let unlockedDeco = this.getUnlockedDeco(gameData)

            const indexData = customData.split(':')
            const cateIndex = parseInt(indexData[0])
            const typeIndex = parseInt(indexData[1])

            const selectItem = (unlockedDeco) => {
                const cate = CATE_NAME[cateIndex]
                const type = TYPE_NAME[typeIndex]
                const lvl = UNLOCK_LEVEL[cateIndex][typeIndex]
                const price = PRICE[cateIndex][typeIndex]

                const unlockUI = (itemNode) => {
                    itemNode.tick.active = true
                    itemNode.require.active = false
                    itemNode.payment.active = false
                    itemNode.ads.active = false
                }

                if (unlockedDeco[cate][type]) {
                    this.changeDeco(cateIndex, typeIndex, (currentDeco) => {
                        this.setUnlockItems()
                    })

                } else if (unlockedLevel > lvl) {
                    if (price === -1) {
                        return this.showAdsReward(() => {
                            unlockUI(this._itemNodes[cateIndex][typeIndex])
                            this.unlockDeco(cateIndex, typeIndex)
                        })
                    }

                    if (price <= money) {
                        this._confirmDeco = { cateIndex, typeIndex }
                        this.showConfirm(true)
                    }

                    if (price > money) {
                        this.showAdsMoney(true)
                    }
                }
            }

            if (!unlockedDeco) {
                this.setUnlockItems((ud) => {
                    selectItem(ud)
                })
            } else {
                selectItem(unlockedDeco)
            }
        })
    }

    private unlockDeco(cateIndex, typeIndex, cb=null) {
        const cate = CATE_NAME[cateIndex]
        const type = TYPE_NAME[typeIndex]

        Data.getData(Data.FACEBOOK_KEY, (err, gameData) => {
            let unlockedDeco = this.getUnlockedDeco(gameData)

            const unlock = (unlockedDeco) => {
                unlockedDeco[cate][type] = true
                gameData.unlockedDeco = unlockedDeco

                Data.saveData(gameData, Data.FACEBOOK_KEY, err => {
                    GameManager.logEvent("decor_" + CATE_NAME[cateIndex] + "_" + TYPE_NAME[typeIndex])

                    this.changeDeco(cateIndex, typeIndex, (currentDeco) => {
                        this.setUnlockItems((ud) => {
                            cb && cb(unlockedDeco)
                        })
                    })
                })
            }

            if (!unlockedDeco) {
                this.setUnlockItems((ud) => {
                    unlock(ud)
                })
            } else {
                unlock(unlockedDeco)
            }

        })
        
    }

    changeDeco(cateIndex, typeIndex, cb=null) {
        const cate = CATE_NAME[cateIndex]
        const type = TYPE_NAME[typeIndex]

        Data.getData(Data.FACEBOOK_KEY, (err, gameData) => {
            const currentDeco = gameData.currentDeco || {}

            currentDeco[cate] = { 'cateIndex': cateIndex, 'type': type, }
            gameData.currentDeco = currentDeco
            Data.saveData(gameData, Data.FACEBOOK_KEY, (err) => {
                cb && cb(currentDeco)
            })

            if (!type) {
                this._homeController.setDecoration(cateIndex, null)
            }

            cc.resources.load(
                `Shop/${type}/${cate}_${type}`,
                cc.SpriteFrame,
                (err, sFrame) => {
                    if (err) {
                        sFrame = null
                    }

                    this._homeController.setDecoration(cateIndex, sFrame)
                },
            )
        })
        
    }

    showAdsReward(cb) {
        AdsManager.getInstance().showRewardedAds(err => {
            if (!err) cb()
        })
    }

    showEffect(node) {
        node.scale = 0
        tween(node).to(.0, {scale: 1}).start()
    }

    showAdsMoney(isShow=false) {
        this.overlay.active = isShow
        this.adsMoney.active = isShow
        this.back.active = isShow

        if (isShow) {
            this.showEffect(this.adsMoney)
            this.showEffect(this.back)
        }
    }

    showConfirm(isShow=false) {
        this.overlay.active = isShow
        this.confirm.active = isShow
        this.back.active = isShow

        if (isShow) {
            this.showEffect(this.confirm)
            this.showEffect(this.back)
        }
    }

    onClosePopup(e) {
        this.showAdsMoney(false)
        this.showConfirm(false)
    }

    onAds(e) {
        this.showAdsMoney(false)
        this.showAdsReward(() => {
            this._gameManager.addMoney(200, (data) => {
                this.setUnlockItems()
            })
        })
    }

    onConfirm(e) {
        const { cateIndex, typeIndex } = this._confirmDeco
        const itemNode = this._itemNodes[cateIndex][typeIndex]

        itemNode.tick.active = true
        itemNode.require.active = false
        itemNode.payment.active = false
        itemNode.ads.active = false

        const price = PRICE[cateIndex][typeIndex]
        this._gameManager.addMoney(-price, (data) => {
            this.unlockDeco(cateIndex, typeIndex)
        })

        this.showConfirm(false)
    }

    static checkBuyableItem(level, money, unlockedDeco) {
        if (!unlockedDeco) {
            return false;
        }
        for (let i = 0; i < PRICE.length; i++) {
            const subPrice = PRICE[i]
            const unlockedCate = unlockedDeco[CATE_NAME[i]]

            for (let j = 0; j < subPrice.length; j++) {
                const itemPrice = subPrice[j]
                const itemRequireLvl = UNLOCK_LEVEL[i][j]
                const unlockedType = unlockedCate[TYPE_NAME[j]]
                if (level >= itemRequireLvl
                        && itemPrice > 0
                        && itemPrice <= money
                        && !unlockedType
                ) {
                    return true
                }
            }
        }

        return false
    }
}
