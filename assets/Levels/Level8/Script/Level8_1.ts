import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    PULL,
    CREAK,
    LOCK,
    UNLOCK,
    SAW,
    DOOR_SLIDE,
    ALERT
  }
@ccclass
export default class Level8_1 extends LevelBase {

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    onDisable(): void {
        super.onDisable();
    }

    initStage(): void {
        super.initStage();
        this.lupin.node.scaleX = 1;
        this.otherSprite[0].node.active = true;
        this.otherSprite[1].node.active = true;
        this.otherSprite[1].node.position = cc.v3(335, -108);
        this.otherSprite[2].node.active = false;
        this.setLupin(cc.v2(370, -350), "emotion/nervous", "general/stand_thinking");
        this.setOtherSpine(this.otherSpine[1], cc.v2(-260, -545), "prisoner/level_8/help_ask", "prisoner/level_8/help_ask");
        this.setOtherSpine(this.otherSpine[0], cc.v2(-800, -350), null, "police/general/run");
        this.otherSpine[1].node.scaleX = -1;
        this.otherSpine[0].node.scaleX = -1;
        tween(this.node).delay(2).call(() => {
            this.showOptionContainer(true);
        }).start();
    }

    runOption1(): void {
        let isTrue = true;
        this.otherSpine[1].setMix("prisoner/level_8/dance", "prisoner/level_8/help_ask", 0.3);
        tween(this.lupin.node).delay(1)
            .call(() => {
                this.lupin.setAnimation(0, "emotion/excited", false);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "emotion/excited" && isTrue)
                    {
                        isTrue = false;
                        let isTrue1 = true;
                        this.otherSprite[1].node.active = false;
                        this.otherSprite[2].node.active = true;
                        this.lupin.setAnimation(1, "level_8/bar_break", false);
                        this.playSound(SOUND.PULL, false, 0)
                        this.playSound(SOUND.CREAK, false, 5)
                        this.otherSpine[1].setAnimation(1, "prisoner/level_8/dance", true);
                        this.lupin.setCompleteListener(track => {
                            if (track.animation.name == "level_8/bar_break" && isTrue1)
                            {
                                isTrue1 = false;
                                this.otherSpine[1].setAnimation(1, "prisoner/level_8/help_ask", true);
                                tween(this.node).delay(1)
                                    .call(() => {
                                        this.showFail(this.selected);
                                    })
                                    .start();
                            }
                        })
                    }
                })
            })
            .start();
    }

    runOption2(): void {
        let isTrue = true;
        this.otherSpine[1].setMix("prisoner/level_8/dance", "prisoner/level_8/help_ask", 0.3);
        tween(this.shadow).delay(1).to(0.5, {opacity: 255})
            .call(() => {
                this.otherSprite[0].node.active = false;
                this.lupin.setAnimation(0, "emotion/excited", false);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "emotion/excited" && isTrue)
                    {
                        isTrue = false;
                        this.playSound(SOUND.LOCK, false, 0.5)
                        this.playSound(SOUND.UNLOCK, false, 1.5)
                        this.playSound(SOUND.DOOR_SLIDE, false, 3)
                        this.lupin.node.scaleX = -1;
                        this.lupin.setAnimation(0, "emotion/angry", true);
                        this.lupin.setAnimation(1, "level_8/lock_open", true);
                        this.otherSpine[1].setAnimation(1, "prisoner/level_8/dance", true);
                        tween(this.otherSprite[1].node).delay(3)
                            .by(1, {position: cc.v3(-520, 0)}, {easing: "cubicIn"})
                            .call(() => {
                                this.otherSpine[1].setAnimation(0, "prisoner/level_8/dance1", true);
                                this.otherSpine[1].setAnimation(1, "prisoner/level_8/dance1", true);
                                this.lupin.setAnimation(0, "emotion/excited", false);
                                this.lupin.setAnimation(1, "general/win_2.1", true);
                            })
                            .delay(2)
                            .call(() => {
                                this.onPass();
                            })
                            .start();
                    }
                })
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption3(): void {
        let isTrue = true;
        this.otherSpine[1].setMix("prisoner/level_8/dance", "prisoner/level_8/help_ask", 0.3);
        tween(this.lupin.node).delay(1)
            .call(() => {
                this.lupin.setAnimation(0, "emotion/excited", false);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "emotion/excited" && isTrue)
                    {
                        isTrue = false;
                        this.lupin.timeScale = 1.5;
                        this.playSound(SOUND.SAW, false, 0.3)
                        this.playSound(SOUND.ALERT, false, 2)
                        
                        this.lupin.setAnimation(1, "level_8/saw_doing", true);
                        this.otherSpine[1].setAnimation(1, "prisoner/level_8/dance", true);
                        tween(this.otherSpine[0].node).by(2, {position: cc.v3(600, 0)})
                            .call(() => {
                                this.otherSpine[0].setAnimation(0, "police/general/gun_raise", false);
                                this.otherSpine[0].setAnimation(1, "police/general/gun_raise", false);
                            })
                            .delay(0.5)
                            .call(() => {
                                cc.audioEngine.stopAllEffects()
                                this.lupin.node.scaleX = -1;
                                this.lupin.setAnimation(0, "emotion/fear_2", false);
                                this.lupin.setAnimation(1, "general/back", false);
                                this.otherSpine[1].setAnimation(1, "prisoner/level_8/help_ask", true);
                            })
                            .delay(1)
                            .call(() => {
                                this.showFail(this.selected);
                            })
                            .start();
                    }
                })
            })
            .start();
    }
}
