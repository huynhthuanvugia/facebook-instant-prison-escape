import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND{
    BEEP,
    ALARM,
    BUZZ_ERROR,
    CELL_DROP,
    DOOR_SLIDE,
    ALERT
}
@ccclass
export default class Level8_3 extends LevelBase {

    initStage(): void {
        super.initStage();
        this.setStatus();
        this.setAction();
        this.lupin.setMix("general/walk_slow", "general/stand_thinking", 0.3);
        this.otherSpine[0].setMix("police/level_8/sleeping", "police/general/gun_raise", 0.1);
    }

    setStatus(): void {
        this.background.position = cc.v3(540.5, 0);
        this.camera2d[0].position = cc.v3(0, 0);
        this.otherSprite[0].node.active = true;
        this.otherSprite[1].node.active = true;
        this.otherSprite[2].node.active = true;
        this.otherSprite[3].node.position = cc.v3(845, -145);
        this.otherSprite[4].node.opacity = 0;
        this.otherSprite[5].node.position = cc.v3(820, 1500);
        this.otherSpine[0].node.zIndex = 100;
        this.otherSprite[6].node.zIndex = 99;
        this.otherSprite[3].node.position = cc.v3(845, -145);
        this.lupin.node.scaleX = 1;
        this.setLupin(cc.v2(-750, -650), "general/walk_slow", "emotion/fear_1");
        this.otherSpine[0].node.scaleX = 1;
        this.otherSpine[0].setAnimation(0, "police/level_8/sleeping", true);
    }

    setAction(): void {
        tween(this.lupin.node).by(3, {position: cc.v3(350, 0)})
            .call(() => {
                this.lupin.setAnimation(0, "general/stand_thinking", false);
                this.lupin.setAnimation(1, "emotion/excited", true);
            })
            .start();
        tween(this.camera2d[0]).by(2, {position: cc.v3(390, 0)})
            .delay(2)
            .by(1.5, {position: cc.v3(500, 0)})
            .by(1.5, {position: cc.v3(-500, 0)})
            .call(() => {
                tween(this.node).delay(2).call(() => {
                    this.showOptionContainer(true);
                }).start();
            }).start();
    }

    runOption1(): void {
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this.camera2d[0].position = cc.v3(1000, 0);
                this.lupin.node.position = cc.v3(800, -500);
                this.otherSprite[1].node.active = false;
                this.lupin.clearTrack(1);
                this.playSound(SOUND.BUZZ_ERROR, false, 1)
                this.lupin.setAnimation(0, "level_8/card_scan_red", false);
                this.lupin.timeScale = 0.8;
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_8/card_scan_red")
                    {
                        this.lupin.setCompleteListener(null);
                        this.lupin.timeScale = 1;
                        this.lupin.node.scaleX = -1;
                        this.playSound(SOUND.CELL_DROP, false, 0.5)
                        tween(this.otherSprite[5].node).delay(0.2).by(1, {position: cc.v3(0, -1760)}, {easing: "cubicIn"})
                            .call(() => {
                                this.playSound(SOUND.ALERT, false, 0)
                                this.otherSpine[0].node.scaleX = -1;
                                this.otherSpine[0].setAnimation(0, "police/general/gun_raise", true);
                                this.otherSpine[0].node.zIndex = 100;
                                this.otherSprite[6].node.zIndex = 101;
                                this.lupin.node.scaleX = -1;
                                this.lupin.setAnimation(0, "general/back", false);
                                this.lupin.setAnimation(1, "emotion/fear_1", false);
                                tween(this.node).delay(1).call(() => {
                                        this.showContinue();
                                    }).start();
                            })
                            .start();
                    }
                });

            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption2(): void {
        tween(this.shadow).to(0.5, {opacity: 255})
        .call(() => {
            this.camera2d[0].position = cc.v3(1000, 0);
            this.lupin.node.position = cc.v3(800, -500);
            this.otherSprite[2].node.active = false;
            this.lupin.clearTrack(1);
            this.lupin.timeScale = 0.8;
            this.lupin.setAnimation(0, "level_8/card_scan_yellow", false);
            this.playSound(SOUND.BEEP, false, 1)
            this.lupin.setCompleteListener(track => {
                if (track.animation.name == "level_8/card_scan_yellow")
                {
                    this.lupin.setCompleteListener(null);
                    this.lupin.timeScale = 1;
                    this.playSound(SOUND.DOOR_SLIDE, false, 0)
                    tween(this.otherSprite[3].node).by(1, {position: cc.v3(350, 0)})
                        .call(() => {
                            this.lupin.node.scaleX = -1;
                            this.lupin.setAnimation(1, "emotion/whistle", true);
                            this.lupin.setAnimation(0, "emotion/whistle", true);
                            tween(this.node).delay(1.5)
                                .call(() => {
                                    let isTrue1 = true;
                                    this.lupin.node.scaleX = 1;
                                    this.lupin.setAnimation(0, "general/run", true);
                                    tween(this.lupin.node).by(1, {position: cc.v3(500, 0)}).start();
                                            tween(this.node).delay(1).call(() => {
                                                    this.showSuccess(this.selected);
                                                }).start();
                                })
                                .start();
                        }).start();
                }
            });

        })
        .to(0.5, {opacity: 0})
        .start();
    }

    runOption3(): void {
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this.camera2d[0].position = cc.v3(1000, 0);
                this.lupin.node.position = cc.v3(800, -500);
                this.otherSprite[0].node.active = false;
                this.lupin.clearTrack(1);
                this.playSound(SOUND.BUZZ_ERROR, false, 1)
                this.lupin.timeScale = 0.8;
                this.lupin.setAnimation(0, "level_8/card_scan_green", false);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_8/card_scan_green")
                    {
                        this.lupin.setCompleteListener(null);
                        this.lupin.timeScale = 1;
                        tween(this.node).call(() => {
                            this.otherSprite[4].node.opacity = 150;
                            this.playSound(SOUND.ALARM, false, 0)
                            this.otherSprite[4].node.getChildByName("bg_alarm").x = -cc.view.getVisibleSize().width;
                            tween(this.otherSprite[4].node).call(() => {
                                    this.otherSpine[0].node.scaleX = -1;
                                    tween(this.otherSpine[0]).delay(1).call(() => {
                                        this.playSound(SOUND.ALERT, false, 0)
                                        this.otherSpine[0].setAnimation(0, "police/general/gun_raise", true);
                                    }).start();
                                    this.otherSpine[0].node.zIndex = 100;
                                    this.otherSprite[6].node.zIndex = 101;
                                    this.lupin.node.scaleX = -1;
                                    this.lupin.setAnimation(0, "general/back", false);
                                    this.lupin.setAnimation(1, "emotion/fear_1", true);
                                    tween(this.node).delay(3).call(() => {
                                            this.showContinue();
                                        }).start();
                                })
                                .repeat(4, 
                                    tween().to(0.5, {opacity: 255}, {easing: "cubicOut"}).to(0.5, {opacity: 50}, {easing: "cubicIn"})
                                )
                                .start();
                        })
                        .start();
                    }
                });

            })
            .to(0.5, {opacity: 0})
            .start();
    }
}
