import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";
import EventManager from "../../../Scripts/EventManager";

enum SOUND {
    WASHING_MACHINE,
    TIPTOE,
    THROW,
    ALERT
  }

const {ccclass, property} = cc._decorator;

var tween = cc.tween;

@ccclass
export default class Level4_1 extends LevelBase {

    @property(sp.Skeleton)
    lupinRotate: sp.Skeleton = null;

    onLoad(): void {
        super.onLoad();
    }

    onEnable(): void {
        super.onEnable();
        this.setBeginSceen(this.node);
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    onDisable(): void {
        super.onDisable();
    }

    initStage(): void {
        super.initStage();
        this.otherSprite[0].node.active = true;
        this.actionBegin();
        this.setCamera(this.camera2d[0], cc.v2(0, 0), 1.0);
    }
    actionBegin(): void {
        this.showOptionContainer(false);
        this.otherSprite[2].node.active = true;
        this.otherSpine[0].node.active = true;
        this.otherSpine[2].node.active = false;
        this.otherSpine[0].clearTrack(0);
        this.setOtherSpine(this.otherSpine[0], cc.v2(1460, -550), null, "police/level_4/stand");
        this.lupin.node.active = true;
        this.setLupin(cc.v2(0, -460), "emotion/worry", "general/stand_thinking");
        this.otherSprite[1].node.active = true;
        this.otherSprite[1].node.position = cc.v3(480, -200);
        this.setCamera(this.camera2d[0], cc.v2(0, 0), 1);
        this.runCamera();
        this.setTimeScaleAllSpine(1);
    }

    runCamera(): void {
        tween(this.camera2d[0]).to(3, {position: cc.v3(1400, 0)})
                            .delay(0.5)
                            .to(0.7, {position: cc.v3(0, 0)})
                            .delay(1)
                            .call(() => {
                                this.showOptionContainer(true);
                            })
                            .start();
    }
    runOption1(): void {
        tween(this.shadow).to(0.3, {opacity: 255})
                        .call(() => {
                            this.lupin.node.active = false;
                            this.otherSprite[1].node.active = false;
                            this.setCamera(this.camera2d[0], cc.v2(440, 0), 1.5);
                            this.setOtherSpine(this.otherSpine[0], cc.v2(805, -290), null, null);
                            this.setOtherSpine(this.otherSpine[1], null, "level_11/fight_insured", "level_4/rotate");
                            this.otherSprite[0].node.active = false;
                            this.playSound(SOUND.WASHING_MACHINE, false, 0)
                        })
                        .delay(0.5)
                        .to(0.3, {opacity: 0})
                        .delay(3)
                        .call(() => {
                            this.stopAction();
                            this.showFail(this.selected);
                        })
                        .start();
    }

    runOption2(): void {
        tween(this.shadow).to(0.3, {opacity: 255})
                        .call(() => {
                            this.setCamera(this.camera2d[0], cc.v2(420, -250), 1.5);
                            this.setOtherSpine(this.otherSpine[0], cc.v2(1460, -550), null, "police/level_4/stand");
                            this.walk();
                        })
                        .delay(0.5)
                        .to(0.3, {opacity: 0})
                        .start();
    }

    runOption3(): void {
        let count = 0;
        this.otherSpine[0].setMix("police/level_4/dirty_trousers_open", "police/level_4/stand_think", 0.2);
        tween(this.shadow).to(0.3, {opacity: 255})
                        .call(() => {
                            this.lupin.node.active = false;
                            this.otherSprite[1].node.active = false;
                            this.setCamera(this.camera2d[0], cc.v2(-170, -230), 1.5);
                            this.otherSpine[2].node.active = true;
                            this.otherSpine[0].setAnimation(0, "police/level_4/dirty_trousers_open", false);
                            this.otherSpine[0].setAnimation(1, "police/level_4/dirty_trousers_open", false);
                            this.otherSpine[0].node.position = cc.v3(-200, -490);
                            this.setOtherSpine(this.otherSpine[2], cc.v2(-50, -400), "emotion/fear_1", "level_4/hide");
                            this.playSound(SOUND.THROW, false, 1.7)
                            this.otherSpine[0].setCompleteListener(trackEntry => {
                                if (trackEntry.animation.name == "police/level_4/dirty_trousers_open" && count < 1)
                                {
                                    ++count;
                                    this.playSound(SOUND.ALERT, false, 0)

                                    this.otherSpine[0].clearTrack(0);
                                    this.otherSpine[0].addAnimation(1, "police/level_4/gun_raise3", false);
                                } 
                                else if (trackEntry.animation.name == "police/level_4/gun_raise3")
                                {
                                    tween(this.node).call(() => {
                                                        this.stopAction();
                                                        this.showFail(this.selected);
                                                    })
                                                    .start();
                                }
                            });
                        })
                        .delay(0.5)
                        .to(0.3, {opacity: 0})
                        .start();
    }

    walk(): void {
        let count = 0;
        this.lupin.setMix("general/walk", "level_4/tiptoed", 0.5);
        this.lupin.setMix("level_4/tiptoed", "general/win_2.1", 0.5);
        this.setLupin(cc.v2(-50, -460), "emotion/excited", "general/walk");
        tween(this.lupin.node).to(3, {position: cc.v3(550, -460)})
                            .call(() => {
                                this.playSound(SOUND.TIPTOE, false, 0)
                                this.lupin.setAnimation(1, "level_4/tiptoed", true);
                                tween(this.camera2d[0]).by(4.9, {position: cc.v3(2150, 0)}).start();
                                tween(this.otherSprite[1].node).by(5, {position: cc.v3(2000, 0)}).start();
                                tween(this.lupin.node).by(5, {position: cc.v3(2000, 0)})
                                                    .call(() => {
                                                        this.lupin.node.position = cc.v3(2600, -450);
                                                        this.lupin.setAnimation(1, "general/win_2.1", true);
                                                    }).start();
                            })
                            .start();
        this.lupin.setCompleteListener(trackEntry => {
            if (trackEntry.animation.name == "general/win_2.1" && count < 1)
            {
                ++count;
                tween(this.lupin.node).delay(1)
                                    .call(() => {
                                        this.lupin.setAnimation(1, "general/run", true);
                                        this.lupin.setAnimation(0, "emotion/worry", false);
                                    })
                                    .by(1, {position: cc.v3(650, -70)})
                                    .call(() => {
                                        this.onPass();
                                    })
                                    .start();
            }
        })
    }
}
