import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";
import EventManager from "../../../Scripts/EventManager";


const {ccclass, property} = cc._decorator;

const tween = cc.tween;

var ui = null;
enum SOUND {
    ELECTROCUTE,
    EXPLOSION,
    JUMP
  }

@ccclass
export default class Level4_2 extends LevelBase {

    onLoad(): void {
        super.onLoad();
        ui = this._gameManager._ui;
    }

    onEnable(): void {
        this._gameManager._levelCurrent.level = this.levelCurrent;
        super.onEnable();
        this.initStage();
    }

    onDisable(): void {
        super.onDisable();
    }

    initStage(): void {
        super.initStage();
        EffectManager.showUI(true);
        ui.showUIEndGame(false);
        this.arrScene.forEach(scene => {
            scene.active = false;
        });
        this._gameManager.optionContainer.getComponent("OptionController").resetOption();

        this.clickOption = true;
        
        this.currentId = 0;

        this.resetUIResult();
        this.setLupin(cc.v2(135, -300), "general/stand_nervous", "general/stand_thinking");
        this.lupin.setAnimation(3, "emotion/idle", true);
        this.otherSprite[0].node.active = true;
        this.otherSprite[1].node.active = false;
        // this.camera2d[0].active = false;
        // this.camera2d[1].active = true;
        this.lupin.node.active = true;
        this.showOptionContainer(false);
        tween(this.camera2d[0]).by(3, {position: cc.v3(750, 0)})
            .delay(1)
            .by(1, {position: cc.v3(-750, 0)})
            .call(() => {
                tween(this.node).delay(2).call(() => {
                    this.showOptionContainer(true);
                }).start();
            })
            .start();
    }

    runOption1(): void {
        var isTrue = true;
        tween(this.shadow).to(0.3, {opacity: 255})
                    .delay(0.5)
                    .call(() => {
                        this.camera2d[1].active = false;
                        this.setCamera(this.camera2d[0], cc.v2(900, 0), 1.5);
                        this.camera2d[1].active = true;
                        this.walk();
                    })
                    .to(0.3, {opacity: 0})
                    .start();
    }

    walk():void {
        this.otherSprite[1].node.active = true;
        this.setLupin(cc.v2(135, -300), "emotion/excited", "general/walk");
        tween(this.lupin.node).by(4, {position: cc.v3(710, 0)})
                            .call(() => {
                                this.playSound(SOUND.EXPLOSION, false,0)
                                this.lupin.setAnimation(1, "fx/explosive2", false);
                            })
                            .delay(3)
                            .call(() => {
                                this.showContinue();
                            })
                            .start();
                            
    }

    runOption2(): void {
        var isTrue = true;
        this.setCamera(this.camera2d[0], cc.v2(0, 0), 1);
        this.camera2d[1].active = false;
        this.camera2d[1].active = true;
        tween(this.shadow).delay(0.5)
                    .call(() => {
                        this.lupin.setAnimation(1, "general/walk", true);
                        tween(this.camera2d[0]).delay(0.2).by(1.8, {position: cc.v3(275, 120)}).start();
                        tween(this.lupin.node).by(2, {position: cc.v3(275, 120)})
                                .call(() => {
                                    this.lupin.setAnimation(0, "emotion/sinister", false);
                                    this.lupin.setAnimation(1, "general/stand_nervous", false);
                                })
                                .start();
                    })
                    .start();
        this.lupin.setCompleteListener(trackEntry => {
            if (trackEntry.animation.name == "emotion/sinister") {
                this.lupin.clearTracks();
                this.lupin.setAnimation(1, "general/climb", false);
            }
            else if (trackEntry.animation.name == "general/climb") {
                this.playSound(SOUND.ELECTROCUTE, false,0)
                this.lupin.addAnimation(1, "general/electric_die", true);
            }
            else if (trackEntry.animation.name == "general/electric_die" && isTrue) {
                isTrue = false;
                tween(this.node).delay(2)
                                .call(() => {
                                    this.showContinue();
                                })
                                .start();
            }
        });
    }

    runOption3(): void {
        var isTrue = true;
        this.setCamera(this.camera2d[0], cc.v2(0, 0), 1);
        this.camera2d[1].active = false;
        this.camera2d[1].active = true;
        tween(this.node).delay(1)
                    .call(() => {
                        this.lupin.setAnimation(0, "emotion/worry", false);
                        this.lupin.setAnimation(1, "general/walk", true);
                        tween(this.camera2d[0]).delay(0.2).by(2.8, {position: cc.v3(850, 0)}).start();
                        tween(this.lupin.node).by(3, {position: cc.v3(825, -330)})
                                        .call(() => {
                                            this.setLupin(cc.v2(960, -630), "emotion/excited", "general/stand_nervous");
                                        })
                                        .delay(1)
                                        .call(() => {
                                            this.lupin.clearTracks();
                                            this.lupin.timeScale = 0.8;
                                            this.lupin.setAnimation(1, "level_4/upside_down", false);
                                            this.playSound(SOUND.JUMP, false, 0.6)

                                        }).start();
                    })
                    .start();
        this.lupin.setCompleteListener(trackEntry => {
            if (trackEntry.animation.name == "level_4/upside_down") {
                isTrue = false;
                tween(this.node).delay(2)
                                .call(() => {
                                    this.showSuccess(this.selected);
                                })
                                .start();
            }
        });
    }

}
