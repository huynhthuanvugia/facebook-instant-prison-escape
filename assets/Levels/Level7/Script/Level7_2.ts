import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";
import EventManager from "../../../Scripts/EventManager";

enum SOUND {
    THROW,
    TIPTOE,
    SMACK,
    SPIN,
    PAPER,
    PULL,
    SLIP
  }

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

@ccclass
export default class Level7_2 extends LevelBase {

    onEnable(): void {
        super.onEnable();
    }

    onDisable(): void {

    }

    initStage(): void {
        super.initStage();
        this._gameManager.mainCamera.active = false;
        this.background.position = cc.v3(163, 0);
        this.lupin.clearTracks();
        this.setLupin(cc.v2(-650, -230), "emotion/idle", "general/stand_thinking");
        this.lupin.setAnimation(0, "emotion/idle", true);
        this.lupin.setAnimation(1, "emotion/worry", true);
        this.setOtherSpine(this.otherSpine[0], cc.v2(-150, -85), null, "police/level_7/book_work");
        this.setOtherSpine(this.otherSpine[1], cc.v2(0, -54), "level_7/ceilingfan_idle", null);
        this.otherSprite[0].node.parent.position = cc.v3(-390, -250);
        this.otherSprite[0].node.parent.angle = 0;
        tween(this.node).delay(2).call(() => {
            this.showOptionContainer(true);
        }).start();
        this.camera2d[0].active = true;
        this.camera2d[1].active = true;
        this.otherSprite[1].node.position = cc.v3(-417, -155);
        this.otherSprite[1].node.angle = 0;
        this.otherSprite[1].node.active = false;
    }

    runOption1(): void {
        let isTrue = true;
        tween(this.lupin.node).delay(1)
        .call(() => {
            this.lupin.setAnimation(0, "emotion/excited", true);
            this.otherSpine[1].setAnimation(0, "level_7/ceilingfan_rotate", true);
            this.playSound(SOUND.SPIN, false, 0)
            this.playSound(SOUND.PAPER, false, 2)

        })
        .delay(1)
        .call(() => {
            this.lupin.node.zIndex = cc.macro.MAX_ZINDEX - 100;
            this.otherSprite[2].node.zIndex = cc.macro.MAX_ZINDEX;
            this.otherSpine[0].setAnimation(1, "police/level_7/paper_face", false);
            this.otherSpine[0].addAnimation(1, "police/level_7/paper_face_loop", true);
            this.lupin.setMix("general/stand_thinking", "general/walk", 0.3);
            this.lupin.setMix("general/walk", "general/run", 0.3);
            tween(this.lupin.node).delay(2).call(() => {
                    this.lupin.setAnimation(1, "general/walk", true);
                    tween(this.lupin.node).by(0.8, {position: cc.v3(0, -50)}).call(() => {
                                this.setLupin(cc.v2(this.lupin.node.position), "emotion/fear_1", "general/run");
                                tween(this.camera2d[0]).by(2, {position: cc.v3(300, 0)}).start();
                            })
                            .by(3, {position: cc.v3(1500, 0)})
                            .call(() => {
                                this.onPass();
                            })
                            .start();
                })
                .start();
            this.lupin.setCompleteListener(trackEntry => {
                if (trackEntry.animation.name == "level_7/put_cockroach" && isTrue)
                {
                    isTrue = false;
                    this.lupin.setAnimation(1, "general/stand_thinking", false);
                    this.lupin.setAnimation(0, "emotion/excited", true);
                    this.otherSprite[1].node.active = true;
                    tween(this.otherSprite[1].node).bezierTo(0.8, cc.v2(-417, -155), cc.v2(0, -155), cc.v2(0, -255))
                                .delay(0.5)
                                .by(0.3, {angle: 70})
                                .by(3, {position: cc.v3(7, 330)})
                                .delay(0.5)
                                .call(() => {
                                    this.otherSpine[0].setAnimation(1, "police/level_7/kill_cockroach", false);
                                })
                                .delay(0.7)
                                .call(() => {
                                    this.otherSprite[1].node.active = false;
                                    this.lupin.setAnimation(0, "emotion/fear_1", false);
                                    this.lupin.setAnimation(1, "general/back", false);
                                    tween(this.node).delay(1).call(() => {
                                        this.lupin.timeScale = 0;
                                        this.otherSpine.forEach(spine => {
                                            spine.timeScale = 0;
                                        })
                                        tween(this.node).call(() => {
                                            EffectManager.showTick(this.selected, this.getLineCurrent());
                                        })
                                        .delay(1)
                                        .call(() => {
                                            EffectManager.effectSuccess();
                                            EffectManager.showUI(false);
                                            this.lupin.timeScale = 0;
                                            this.otherSpine.forEach(spine => {
                                                spine.timeScale = 0;
                                            });
                                        })
                                        .delay(2)
                                        .call(() => {
                                            EventManager.sendRequestNextStage(this.node);
                                        })
                                        .start();
                                    })
                                    .start();
                                })
                                .start();
                }
            });
        })
        .start();
    }

    runOption2(): void {
        let isTrue = true;
        tween(this.lupin.node).delay(1)
        .call(() => {
            this.lupin.setAnimation(0, "emotion/sinister", false);
        })
        .delay(1)
        .call(() => {
            this.lupin.node.zIndex = cc.macro.MAX_ZINDEX;
            this.lupin.setMix("level_7/put_cockroach", "general/stand_thinking", 0.3);
            this.lupin.setAnimation(1, "level_7/put_cockroach", false);
            this.lupin.setCompleteListener(trackEntry => {
                if (trackEntry.animation.name == "level_7/put_cockroach" && isTrue)
                {
                    isTrue = false;
                    this.lupin.setAnimation(1, "general/stand_thinking", false);
                    this.lupin.setAnimation(0, "emotion/excited", true);
                    this.otherSprite[1].node.active = true;
                    this.playSound(SOUND.THROW, false, 0.1)
                    tween(this.otherSprite[1].node).bezierTo(0.8, cc.v2(-417, -155), cc.v2(0, -155), cc.v2(0, -255))
                                .delay(0.5)
                                .by(0.3, {angle: 70})
                                .call(()=>{
                                    this.playSound(SOUND.TIPTOE, false, 0)
                                })
                                .by(3, {position: cc.v3(7, 330)})
                                .delay(0.5)
                                .call(() => {
                                    this.otherSpine[0].setAnimation(1, "police/level_7/kill_cockroach", false);
                                    cc.audioEngine.stopAllEffects()
                                    this.playSound(SOUND.SMACK, false, 0.2)
                                })
                                .delay(0.7)
                                .call(() => {
                                    this.otherSprite[1].node.active = false;
                                    this.lupin.setAnimation(0, "emotion/fear_1", false);
                                    this.lupin.setAnimation(1, "general/back", false);
                                    tween(this.node).delay(1).call(() => {
                                        this.lupin.timeScale = 0;
                                        this.otherSpine.forEach(spine => {
                                            spine.timeScale = 0;
                                        })
                                        tween(this.node).delay(1).call(() => {
                                            this.lupin.timeScale = 0;
                                            this.otherSpine.forEach(spine => {
                                                spine.timeScale = 0;
                                            })
                                            tween(this.node).call(() => {
                                                this.showContinue();
                                            })
                                            .start();
                                        })
                                        .start();
                                    })
                                    .start();;
                                })
                                .start();
                }
            });
        })
        .start();
    }

    runOption3(): void {
        tween(this.lupin.node).delay(1)
        .call(() => {
            this.lupin.setAnimation(0, "emotion/sinister", false);
        })
        .delay(1)
        .call(() => {
            this.lupin.node.zIndex = cc.macro.MIN_ZINDEX;
            this.lupin.setMix("level_7/push_book", "general/back", 0.3);
            this.lupin.timeScale = 2;
            this.playSound(SOUND.PULL, false, 0.1)
            
            this.scheduleOnce(()=>{
                cc.audioEngine.stopAllEffects()
                this.playSound(SOUND.SLIP, false, 0)
            }, 3)
            this.lupin.setAnimation(1, "level_7/push_book", false);
            this.lupin.addAnimation(1, "general/back", false);
            tween(this.otherSprite[0].node.parent).delay(0.3).by(0.5, {angle: -10}, {easing: "cubicInOut"})
                                .by(1, {angle: 20}, {easing: "cubicInOut"})
                                .call(() => {
                                    tween(this.node).delay(1).call(() => {
                                        this.lupin.timeScale = 0;
                                        this.otherSpine.forEach(spine => {
                                            spine.timeScale = 0;
                                        })
                                        tween(this.node).call(() => {
                                            this.showContinue();
                                        })
                                        .start();
                                    })
                                    .start();
                                })
                                .by(1, {angle: -20}, {easing: "cubicInOut"})
                                .call(() => {
                                    this.lupin.timeScale = 1;
                                    this.lupin.addAnimation(0, "emotion/fear_1", false);
                                })
                                .by(1.2, {angle: 30}, {easing: "cubicInOut"})
                                .start();
        })
        .start();
    }
}
