import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND{
    SWOOSH,
    THROW,
    SIGH,
    TORCH,
    WORD_BREAK
}

@ccclass
export default class Level7_1 extends LevelBase {

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage() {
        super.initStage();

        this._gameManager.mainCamera.active = false;

        this.camera2d[0].position = cc.v3(0, 0);
        this.camera2d[0].active = true;
        this.camera2d[1].active = true;

        this.setLupin(cc.v2(-231, -580), 'emotion/sad', 'general/stand_thinking')
        this.lupin.timeScale = 1
        this.lupin.setCompleteListener(null)

        this.otherSprite[0].node.position = cc.v3(387, 516)
        this.otherSprite[1].node.active = true
        this.otherSprite[2].node.active = false
        this.otherSprite[3].node.active = false

        tween(this.node)
            .delay(2)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1() {
        this.setLupin(cc.v2(this.lupin.node.position), 'emotion/excited', 'general/walk')

        tween(this.lupin.node)
            .by(3, {position: cc.v3(500)})
            .call(() => {
                this.lupin.setAnimation(0, 'level_9/door_creep2', true)
                this.lupin.clearTrack(1)
                this.playSound(SOUND.THROW, false, 1)
                tween(this.otherSprite[0].node)
                    .delay(1)
                    .by(.5, {position: cc.v3(0, -265)})
                    .start()
            })
            .by(2, {position: cc.v3(50, 280)})
            .call(() => {
                this.lupin.setAnimation(1, 'emotion/fear_1', false)
                this.lupin.timeScale = 0
            })
            .delay(2)
            .by(0, {position: cc.v3(-50, -280)})
            .call(() => {
                this.lupin.timeScale = 0.5
                this.lupin.setAnimation(0, 'general/stand_thinking', true)
                this.lupin.setAnimation(1, 'emotion/fear_2', false)
            })
            .delay(2)
            .call(() => {
                this.lupin.timeScale = 0
                this.showFail(this.selected)
            })
            .start()
    }

    runOption2() {
        this.setLupin(cc.v2(this.lupin.node.position), 'emotion/excited', 'general/walk')
        let count = 0

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_9/weld_tired') {
                count++
                if (count === 3) {
                    this.lupin.timeScale = 0
                    this.showFail(this.selected)
                }
            }
        })

        tween(this.lupin.node)
            .by(2, {position: cc.v3(340, 50)})
            .call(() => {
                this.playSound(SOUND.TORCH, false, 0)
                this.setLupin(cc.v2(this.lupin.node.position), 'emotion/excited', 'level_9/weld')
            })
            .delay(4)
            .call(() => {
                this.playSound(SOUND.SIGH, false, 0)
                this.setLupin(cc.v2(this.lupin.node.position), 'emotion/tired', 'level_9/weld_tired')
            })
            .start()
    }

    runOption3() {
        let count = 0

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'general/kick1') {
                this.setLupin(cc.v2(this.lupin.node.position), 'emotion/sinister', 'general/win_1.1')
            }

            if (track.animation.name === 'general/win_1.1') {
                count++

                if (count === 2) {
                    this.onPass()
                }
            }
        })

        tween(this.lupin.node)
            .delay(1)
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'emotion/angry', 'general/run')
            })
            .by(1, {position: cc.v3(370)})
            .call(() => {
                this.playSound(SOUND.SWOOSH, false, 0.4)
                this.playSound(SOUND.WORD_BREAK, false, 0.7)
                this.lupin.setAnimation(1, 'general/kick1', false)
            })
            .delay(.6)
            .call(() => {
                this.otherSprite[1].node.active = false
                this.otherSprite[2].node.active = true
                this.otherSprite[3].node.active = true
            })
            .start()
    }
}