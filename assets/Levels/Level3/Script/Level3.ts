import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
var tween = cc.tween;

enum SOUND {
    WOOD_BREAK,
    FREEZE,
    FALL
  }
@ccclass
export default class Level3 extends LevelBase {

    /*
        -60 -400
    */
    onEnable(): void {
        super.onEnable();
        this.setBackground();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
        this.setBackground();
        this.actionBegin();
    }   
    
    setBackground() {
        this.background.position = cc.v3(410, 0);
        this.otherSprite[2].node.position = cc.v3(15, -330);
        this.otherSprite[3].node.position = cc.v3(-170, -465);
        this.otherSprite.forEach(sprite => {
            sprite.node.active = false;
        });
    }
    actionBegin() {
        var count = 0;
        this.lupin.node.active = true;
        this.showOptionContainer(false);
        this.background.position = cc.v3(385, 0);
        this.setLupin(cc.v2(-300, -400), "emotion/nervous", "general/walk_slow");
        tween(this.lupin.node).to(2.5, {position: cc.v3(-60, -400)})
                            .call(() => {
                                this.lupin.setAnimation(1, "level_3/look_down", false);
                                this.lupin.setMix("level_3/look_down_loop", "emotion/idle", 0.3);
                                this.lupin.addAnimation(1, "level_3/look_down_loop", false);
                                this.lupin.setAnimation(0, "emotion/fear_1", false);
                                this.lupin.setCompleteListener(trackEntry => {
                                    if (trackEntry.animation.name == "level_3/look_down_loop")
                                    {
                                        this.lupin.addAnimation(1, "emotion/idle", true);
                                        this.lupin.setAnimation(0, "emotion/fear_2", false);
                                        this.lupin.setCompleteListener(trackEntry => {
                                            if (trackEntry.animation.name == "emotion/fear_2")
                                            {
                                                if (count < 1)
                                                {
                                                    this.showOptionContainer(true);
                                                    ++count;
                                                }
                                            }
                                        });
                                    }
                                })
                            })
                            .start();
        this.lupin.setMix("level_3/lv3_stg1_wood", "general/fall_high", 0.3);
    }
    runOption1(): void {
        tween(this.shadow).to(0.3, {opacity: 255})
                        .call(() => {
                            this.otherSprite[0].node.active = true;
                        })
                        .delay(0.5)
                        .call(() => {
                            this.lupin.setAnimation(1, "general/walk", true);
                            tween(this.lupin.node).to(1, {position: cc.v3(100, -330)})
                                                .call(() => {
                                                    this.lupin.timeScale = 1.2;
                                                    this.lupin.setAnimation(1, "level_3/lv3_stg1_wood", true);
                                                    this.lupin.setAnimation(0, "emotion/worry", true);
                                                    tween(this.background).to(4.2, {position: cc.v3(-100, 0)}).start();
                                                    tween(this.lupin.node).to(4, {position: cc.v3(-100, -330)})
                                                                        .delay(1)
                                                                        .call(() => {
                                                                            this.lupin.timeScale = 1;
                                                                            this.playSound(SOUND.FREEZE, false, 0)
                                                                            this.playSound(SOUND.FALL, false, 1)
                                                                            this.lupin.setAnimation(1, "general/fall_high", true);
                                                                            this.lupin.setAnimation(0, "emotion/fear_2", true);
                                                                        })
                                                                        .delay(1)
                                                                        .call(() => {
                                                                            this.crashed();
                                                                        })
                                                                        .delay(1)
                                                                        .call(() => {
                                                                            this.showFail(this.selected);
                                                                        }).start();
                                                }).start();
                        })
                        .to(0.3, {opacity: 0})
                        .start();
    }

    runOption2(): void {
        tween(this.shadow).to(0.3, {opacity: 255})
                        .call(() => {
                            this.otherSprite[1].node.active = true;
                        })
                        .delay(0.5)
                        .call(() => {
                            tween(this.lupin.node).to(0, {position: cc.v3(-50, -330)})
                                                .call(() => {
                                                    this.creep();
                                                }).start();
                        })
                        .to(0.3, {opacity: 0})
                        .start();
    }

    runOption3(): void {
        tween(this.shadow).to(0.3, {opacity: 255})
                        .call(() => {
                            this.otherSprite[2].node.active = true;
                        })
                        .delay(0.5)
                        .call(() => {
                            this.lupin.setAnimation(1, "general/walk", true);
                            tween(this.lupin.node).to(1, {position: cc.v3(200, -330)})
                                                .call(() => {
                                                    this.lupin.setAnimation(1, "level_3/lv3_stg1_wood", true);
                                                    this.lupin.setAnimation(0, "emotion/worry", true);
                                                    this.playSound(SOUND.WOOD_BREAK, false, 3.8)
                                                    tween(this.background).to(4, {position: cc.v3(-100, 0)})
                                                                        .call(function() {
                                                                            this.broken();
                                                                        }.bind(this)).start();
                                                    tween(this.lupin.node).to(3, {position: cc.v3(-100, -330)})
                                                                        .delay(1)
                                                                        .call(() => {
                                                                            this.playSound(SOUND.FALL, false, 0.2)
                                                                            this.lupin.setAnimation(1, "general/fall_high", true);
                                                                            this.lupin.setAnimation(0, "emotion/fear_2", true);
                                                                        })
                                                                        .delay(1)
                                                                        .call(() => {
                                                                            this.crashed();
                                                                        })
                                                                        .delay(1)
                                                                        .call(() => {
                                                                            this.showFail(this.selected);
                                                                        }).start();
                                                }).start();
                        })
                        .to(0.3, {opacity: 0})
                        .start();
    }

    broken(): void {
        this.otherSprite[2].node.active = false;
        this.otherSprite[3].node.active = true;
    }

    crashed(): void {
        tween(this.lupin.node).by(0.5, {position: cc.v3(0, -2000)}).start();
        tween(this.otherSprite[3].node).by(0.5, {position: cc.v3(0, -2000)}).start();
    }

    creep(): void {
        this.lupin.setMix("general/creep", "general/walk", 0.3);
        this.lupin.setAnimation(1, "general/creep", true);
        this.lupin.setAnimation(0, "emotion/worry", true);
        tween(this.lupin.node).by(8, {position: cc.v3(40, 30)}).start();
        tween(this.background).to(8, {position: cc.v3(-385, 0)})
                            .call(() => {
                                this.walk();
                                tween(this.lupin.node).by(2, {position: cc.v3(200, 0)})
                                                    .call(() => {
                                                        this.lupin.setAnimation(1, "general/win_1.1", true);
                                                    })
                                                    .delay(1)
                                                    .call(() => {
                                                        this.showSuccess(this.selected);
                                                    })
                                                    .start();
                            }).start();
    }

    walk(): void {
        this.lupin.setAnimation(1, "general/walk", true);
        this.lupin.setAnimation(0, "emotion/excited", true);
    }
}
