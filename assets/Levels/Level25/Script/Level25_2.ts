import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;

@ccclass
export default class Level25_2 extends LevelBase {

    private _thunder;
    private _rain;
    private _explosion;
    private _beard;
    private _aborigines;

    initStage(): void {
        super.initStage();
        this.setStatus();
        this.setAction();
    }

    setStatus(): void {
        this._thunder = this.otherSpine[2];
        this._rain = this.otherSpine[3];
        this._aborigines = this.otherSpine[1];
        this._explosion = this.otherSpine[0];
        this._beard = this.otherSprite[0];
        this.setLupin(cc.v2(-700, -170), "general/run", "emotion/fear_1");
        this.lupin.setMix("general/run", "general/stand_nervous", 0.3);
        this.lupin.setMix("general/stand_nervous", "general/walk", 0.3)
        this.lupin.node.scaleX = 1;
        this._beard.node.active = false;
        this._rain.node.active = false;
        this._thunder.node.active = false;
        this._aborigines.setAnimation(0, "level_25_2/thodan_idle", true);
        this._aborigines.node.scaleX = -1;
        this._aborigines.node.position = cc.v3(250, -170);
        this._explosion.node.active = false;
    }

    setAction(): void {
        tween(this.lupin.node).to(2, {x: -150})
            .call(() => {
                this.lupin.setAnimation(0, "general/stand_nervous", true);
                this.lupin.setAnimation(1, "level_25_1/mc_worry", false);
                this.lupin.addAnimation(1, "level_25_1/look_back", false);
            })
            .delay(3)
            .call(() => {
                this.lupin.node.scaleX = -1;
                this.lupin.setAnimation(0, "general/walk", true);
                this.lupin.setAnimation(1, "emotion/worry", false);
            })
            .to(1, {position: cc.v3(-150, -65)})
            .call(() => {
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_25_2/mc_see_wanted", false);
                this.lupin.setAnimation(1, "emotion/fear_2", true);
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();
    }

    runOption1(): void {
        this.lupin.setAnimation(0, "level_25_2/mc_sua_anh", false);
        this.lupin.setAnimation(1, "level_25_2/mc_sua_anh", false);
        this.scheduleOnce(() => {
            this._beard.node.active = true;
        }, 1);
        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_25_2/mc_sua_anh") {
                this.lupin.setCompleteListener(null);
                this.lupin.setAnimation(0, "emotion/idle", false);
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_25_2/mc_sua_anh2", false);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_25_2/mc_sua_anh2") {
                        this.lupin.setCompleteListener(null);
                        this.lupin.node.scaleX = 1;
                        this.lupin.clearTrack(1);
                        this.lupin.setAnimation(0, "general/stand_thinking", false);
                        this.lupin.setAnimation(1, "emotion/excited", false);
                        tween(this.node).delay(1)
                            .call(() => {
                                tween(this.lupin.node).call(() => {
                                        this.lupin.setAnimation(0, "general/walk", true);
                                        this.lupin.setAnimation(1, "emotion/idle", false);
                                    })
                                    .to(1, {position: cc.v3(-80, -170)})
                                    .call(() => {
                                        this.lupin.setAnimation(0, "emotion/idle", false);
                                        this.lupin.setAnimation(1, "emotion/happy_2", false);
                                    })
                                    .delay(3)
                                    .call(() => {
                                        this.onPass();
                                    })
                                    .start();
                            })
                            .start();
        
                        tween(this._aborigines.node).delay(3).call(() => {
                                this._aborigines.node.scaleX = 1;
                                this._aborigines.setAnimation(0, "level_25_2/thodan_walking2", true);
                            })
                            .to(0.7, {x: 170})
                            .call(() => {
                                this._aborigines.setMix("level_25_2/thodan_walking2", "level_25_2/thodan_welcome", 0.3);
                                this._aborigines.setAnimation(0, "level_25_2/thodan_welcome", true);
                            })
                            .start();
                        }
                })
            }
        })
    }

    runOption2(): void {
        this.lupin.setAnimation(0, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this.lupin.node.scaleX = 1;
        this.lupin.setAnimation(0, "level_25_2/mc_boimat", false);
        this.lupin.addAnimation(0, "level_25_2/mc_boimat2", true);
        tween(this._aborigines.node).delay(1)
            .call(() => {
                this._aborigines.node.scaleX = 1;
            })
            .start();
        tween(this.node).delay(3)
            .call(() => {
                this._rain.node.active = true;
                this._rain.setAnimation(0, "level_20_1/idle_may2", true);
            })
            .delay(1)
            .call(() => {
                this.lupin.setAnimation(0, "level_25_2/mc_rain", false);
                this.scheduleOnce(() => {
                    this._thunder.node.active = true;
                    this._thunder.setAnimation(0, "fx/lightning", false);
                    this._thunder.setCompleteListener(track => {
                        if (track.animation.name == "fx/lightning") {
                            this._thunder.setCompleteListener(null);
                            this._thunder.node.active = false;
                        }
                    })
                }, 0.7);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_25_2/mc_rain") {
                        this.showContinue();
                        this._rain.timeScale = 1;
                    }
                })
            })
            .start();
    }

    runOption3(): void {
        this.lupin.setAnimation(0, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this._explosion.node.active = true;
        this._explosion.setAnimation(0, "fx/explosion", false);
        tween(this.lupin.node).to(0.3, {opacity: 0})
            .call(() => {
                this.lupin.node.scaleX = 1;
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_25_2/mc_girl_idle", false);
            })
            .to(0.5, {opacity: 255})
            .delay(1)
            .call(() => {
                this._aborigines.node.scaleX = 1;
                this._aborigines.setAnimation(0, "level_25_2/thodan_suprise", false);
                this._aborigines.setCompleteListener(track => {
                    if (track.animation.name == "level_25_2/thodan_suprise") {
                        this._aborigines.setCompleteListener(null);
                        this._aborigines.setAnimation(0, "level_25_2/thodan_heat_walking", true);
                        this.lupin.setAnimation(0, "level_25_2/mc_girl_scare", true);
                        tween(this.lupin.node).by(1, {x: -100})
                            .call(() => {
                                this.lupin.addAnimation(0, "level_25_2/mc_girl_scare1", true);
                            })
                            .start();
                        tween(this._aborigines.node).by(1, {position: cc.v3(-250, 50)})
                            .call(() => {
                                this._aborigines.setAnimation(0, "level_25_2/thodan_talking_weding", false);
                                this._aborigines.addAnimation(0, "level_25_2/thodan_idle2", false);
                            })
                            .delay(2)
                            .call(() => {
                                this.showContinue();
                            })
                            .start();
                    }
                })
            })
            .start();
    }
}
