import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from '../../../Scripts/EffectManager';


const {ccclass, property} = cc._decorator
const tween = cc.tween;

@ccclass
export default class Level30_3 extends LevelBase {

    initStage(): void {
        super.initStage()
        this.setStatus()
        this.setAction()
    }

    setStatus(): void {
        this.lupin.node.active = true
        this.lupin.clearTrack(1)
        this.setLupin(cc.v2(-992, -629), 'level_30_3/mc_riding_Kong', null)
        this.lupin.node.scale = 1

        this.setOtherSpine(this.otherSpine[0], cc.v2(-907, 228), 'level_30_3/witch_bloom_fly', null)
        this.setOtherSpine(this.otherSpine[1], cc.v2(-979, -486), 'kong/kong_runing', null)

        this.otherSpine[0].setCompleteListener(null)
        this.otherSpine[0].node.scaleX = -1

        this.otherSpine[1].node.scale = 2
        this.otherSpine[1].setCompleteListener(null)

        this.otherSpine[2].node.active = false

        cc.Tween.stopAllByTag(201)

        this.otherSprite[0].node.position = cc.v3(0, 0)

        this.otherSprite[1].node.active = false
        this.otherSprite[1].node.position = cc.v3(0, -47)

        this.otherSprite[2].node.active = false
        this.otherSprite[3].node.active = false
    }

    setAction(): void {
        tween(this.lupin.node).to(1, {position: cc.v3(46, -629)}).start()
        tween(this.otherSpine[1].node).to(1, {position: cc.v3(59, -486)}).start()

        tween(this.otherSpine[0].node)
            .delay(2)
            .to(1, {position: cc.v3(-342, 228)})
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        tween(this.otherSprite[0].node)
            .tag(201)
            .delay(1)
            .repeatForever(
                tween()
                    .to(2, {position: cc.v3(-4520, 0)})
                    .set({position: cc.v3(0, 0)})
            )
            .start()
    }

    runOption1(): void {
        let check = false

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_30_3/mc_riding_Kong') {
                if (check) {
                    check = false
                    this.lupin.setAnimation(0, 'level_30_3/mc_call_ufo', false)
                }
            }

            if (track.animation.name === 'level_30_3/mc_call_ufo') {
                this.lupin.setAnimation(0, 'level_30_3/mc_riding_Kong', true)

                EffectManager.hideScene((node) => {
                    this.otherSprite[3].node.active = true
                    this.otherSprite[1].node.active = true

                    EffectManager.showScene()

                    this.scheduleOnce(() => {
                        EffectManager.hideScene((node) => {
                            this.otherSprite[3].node.active = false
                            this.otherSprite[1].node.position = cc.v3(0, 1224)

                            EffectManager.showScene()

                            tween(this.otherSprite[1].node)
                                .to(2, {position: cc.v3(0, 816)})
                                .delay(1)
                                .call(() => {
                                    this.otherSprite[2].node.active = true
                                    this.setLupin(
                                        cc.v2(46, -64),
                                        'general/stand_nervous',
                                        'emotion/whistle',
                                    )

                                    tween(this.lupin.node)
                                        .to(2, {position: cc.v3(46, 1241)})
                                        .start()
                                })
                                .delay(3)
                                .call(() => {
                                    this.otherSprite[2].node.active = false
                                })
                                .to(2, {position: cc.v3(0, 1224)})
                                .call(() => {
                                    cc.Tween.stopAllByTag(201)

                                    this.otherSpine[1].setAnimation(0, 'kong/kong_walking', true)

                                    tween(this.otherSpine[0].node)
                                        .to(1, {position: cc.v3(109, 228)})
                                        .flipX()
                                        .to(2, {position: cc.v3(-882, 228)})
                                        .start()

                                    tween(this.otherSpine[1].node)
                                        .to(4, {position: cc.v3(921, -486)})
                                        .call(() => {
                                            this.onPass()
                                        })
                                        .start()
                                })
                                .start()
                        }, this.node)
                    }, 2)
                }, this.node)
            }
        })

        this.scheduleOnce(() => {
            check = true
        }, 1)
    }

    runOption2(): void {
        this.otherSpine[0].setMix('level_30_3/witch_bloom_attack', 'level_30_2/witch_victory', .3)

        let check = false

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_30_3/mc_riding_Kong') {
                if (check) {
                    check = false
                    this.lupin.setAnimation(0, 'level_30_3/mc_call_drink_potion', false)
                }

            }

            if (track.animation.name === 'level_30_3/mc_call_drink_potion') {
                this.lupin.setAnimation(0, 'level_30_3/mc_riding_Kong', true)

                tween(this.lupin.node)
                    .to(1, {position: cc.v3(57, -512), scale: .2})
                    .to(2, {position: cc.v3(325, -512)})
                    .start()

                tween(this.otherSpine[1].node)
                    .to(1, {scale: .4})
                    .call(() => {
                        cc.Tween.stopAllByTag(201)
                        tween(this.otherSpine[0].node)
                            .to(2, {position: cc.v3(-188, -487)})
                            .start()
                    })
                    .to(2, {position: cc.v3(327, -486)})
                    .call(() => {
                        this.otherSpine[0].setAnimation(0, 'level_30_3/witch_bloom_attack', false)

                        this.scheduleOnce(() => {
                            this.otherSpine[0].setCompleteListener(null)

                            // cc.Tween.stopAllByTag(201)

                            this.otherSpine[0].setAnimation(0, 'level_30_2/witch_victory', true)
                            this.otherSpine[1].setAnimation(0, 'kong/kong_dep_lep', false)
                            this.lupin.node.active = false

                            this.scheduleOnce(() => {
                                this.showContinue()
                            }, 1.5)
                        }, .8)
                    })
                    .start()
            }
        })

        this.scheduleOnce(() => {
            check = true
        }, 1)
    }

    runOption3(): void {
        this.lupin.setMix('level_22_3/funk', 'level_22_3/faint', .3)
        this.otherSpine[1].setMix('kong/kong_angry', 'kong/kong_ready_to_fight', .3)

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'level_30_3/witch_bloom_fly2') {
                this.otherSpine[2].node.active = true
                this.otherSpine[2].setAnimation(0, 'godzilla/godzilla_summon_out', false)
            }
        })

        this.otherSpine[1].setCompleteListener((track) => {
            if (track.animation.name === 'kong/kong_angry') {
                this.otherSpine[1].setCompleteListener(null)
                this.otherSpine[1].setAnimation(0, 'kong/kong_ready_to_fight', true)

                this.scheduleOnce(() => {
                    this.otherSpine[0].setAnimation(0, 'level_30_3/witch_bloom_fly2', false)
                    this.otherSpine[0].addAnimation(0, 'level_30_3/witch_bloom_fly', true)
                }, 1)
            }
        })

        this.otherSpine[2].setCompleteListener((track) => {
            if (track.animation.name === 'godzilla/godzilla_summon_out') {
                this.otherSpine[1].setAnimation(0, 'kong/kong_vs_godzilla', true)
                this.otherSpine[2].setAnimation(0, 'godzilla/godzilla_vs_Kong', true)

                this.scheduleOnce(() => {
                    this.lupin.clearTrack(1)
                    this.lupin.setAnimation(0, 'level_22_3/funk', true)
                }, 1)

                this.scheduleOnce(() => {
                    this.lupin.setAnimation(0, 'level_22_3/faint', false)
                }, 2)

                this.scheduleOnce(() => {
                    this.showContinue()
                }, 3)
            }
        })

        this.scheduleOnce(() => {
            cc.Tween.stopAllByTag(201)
            this.otherSpine[1].node.scaleX = -2
            this.otherSpine[1].setAnimation(0, 'kong/kong_angry', false)

            this.setLupin(cc.v2(367, -583), 'general/stand_nervous', 'emotion/sinister')
            this.lupin.node.scaleX = -1
        }, 1)
    }
}
