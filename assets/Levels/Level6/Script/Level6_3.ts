import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;
 enum SOUND {
    THROW,
    HIT,
    MOUSE,
    MICE,
    THUD,
    SCREAM,
    ALERT
  }

@ccclass
export default class Level6_3 extends LevelBase {

    @property(cc.Node)
    mouseContainer: cc.Node = null;

    onEnable():void {
        super.onEnable();
    }

    onDisable():void {
        super.onDisable();
    }

    initStage(): void {
        super.initStage();
        this.lupin.clearTracks();
        this.mouseContainer.position = cc.v3(1200, -270);
        for (let children of this.mouseContainer.children)
        {
            children.getComponent(sp.Skeleton).timeScale = (Math.floor(Math.random() * 10) + 8) / 10;
        }
        this.otherSpine[0].node.scaleX = 0.8;
        this.background.position = cc.v3(210, 0);
        this.camera2d[0].position = cc.v3(0, 0);
        this.otherSprite[0].node.position = cc.v3(1190, -200);
        this.otherSpine[0].setMix("level_6/run_angry", "level_6/angry", 0.3);
        this.otherSpine[0].setMix("level_6/angry", "level_6/run_angry", 0.3);
        this.lupin.setMix("general/crawl", "general/crawl_idle", 0.3);
        this.setLupin(cc.v2(-380, -270), "emotion/fear_1", "general/crawl");
        this.setOtherSpine(this.otherSpine[0], cc.v2(1200, -270), null, "level_6/run_angry");
        tween(this.node).call(() => {
                tween(this.camera2d[0]).by(2.5, {position: cc.v3(450, 0)}).start();
                this.playSound(SOUND.MOUSE, false, 1)
                tween(this.lupin.node).by(2, {position: cc.v3(480, 0)})
                        .call(() => {
                            tween(this.otherSpine[0].node).by(2, {position: cc.v3(-500, 0)})
                                    .call(() => {
                                        this.otherSpine[0].setAnimation(1, "level_6/angry", true);
                                        tween(this.node).delay(2).call(() => {
                                            this.showOptionContainer(true);
                                        }).start();
                                    }).start();
                            this.lupin.setAnimation(0, "emotion/angry", true);
                            this.lupin.setAnimation(1, "general/crawl_idle", false);
                            this.lupin.setAnimation(2, "emotion/angry", true);
                        })        
                        .start();
            })
            .start();
    }

    runOption1(): void {
        tween(this.lupin.node).delay(1)
        .call(() => {
            this.lupin.setAnimation(1, "level_6/slap", false);
            this.lupin.addAnimation(1, "level_6/slap", false);
            this.lupin.addAnimation(1, "level_6/slap", false);
            this.lupin.addAnimation(1, "level_6/slap", false);
            this.playSound(SOUND.THUD, false, 0.5)
            this.playSound(SOUND.THUD, false, 1.1)
            this.playSound(SOUND.THUD, false, 1.7)
            this.playSound(SOUND.THUD, false, 2.3)
            tween(this.camera2d[0]).delay(0.2).repeat(8, 
                    tween().by(0.1, {position: cc.v3(10, 10)}).by(0.1, {position: cc.v3(-10, -10)})
                )
                .call(() => {
                    this.lupin.setAnimation(0, "emotion/excited", false);
                    this.lupin.setAnimation(2, "emotion/excited", false);
                    this.otherSpine[0].node.scaleX = -0.8;
                    this.otherSpine[0].setAnimation(1, "level_6/run_angry", true);
                    this.playSound(SOUND.ALERT, false, 1.5)
                    tween(this.otherSpine[0].node).by(2, {position: cc.v3(500, 0)})
                            .call(() => {
                                tween(this.otherSprite[0].node).by(1.5, {position: cc.v3(-215, 0)})
                                    .call(() => {
                                        this.showContinue();
                                    })
                                    .start();
                                this.lupin.clearTrack(2);
                                this.lupin.addAnimation(0, "emotion/fear_2", false);
                                // this.lupin.setAnimation(3, "emotion/fear_2", false);
                            }).start();
                })
                .start();
        })
        .start();
    }

    runOption2():void {
        tween(this.lupin.node).delay(1)
            .call(() => {
                this.lupin.setAnimation(0, "emotion/excited", true);
                this.lupin.setAnimation(2, "emotion/excited", true);
                this.playSound(SOUND.THROW, false, 0.8)
                this.playSound(SOUND.HIT, false, 1)
                this.lupin.setAnimation(1, "level_6/throw_rock", false);
                tween(this.otherSpine[0]).delay(0.8).call(() => {
                    this.otherSpine[0].setAnimation(1, "level_6/get_rock", false);
                    this.otherSpine[0].setCompleteListener(track => {
                        if (track.animation.name == "level_6/get_rock")
                        {

                            this.otherSpine[0].setAnimation(1, "level_6/run_angry", true);
                            this.otherSpine[0].node.scaleX = -0.8;
                            tween(this.otherSpine[0].node).by(2, {position: cc.v3(500, 0)})
                                    .call(() => {
                                        this.lupin.clearTrack(0);
                                        this.lupin.clearTrack(2);
                                        this.lupin.addAnimation(0, "emotion/fear_2", false);
                                        this.playSound(SOUND.MICE, false, 1)
                                        this.playSound(SOUND.SCREAM, false, 2.3)

                                        tween(this.mouseContainer).by(2, {position: cc.v3(cc.v3(-650, 0))})
                                            .call(() => {
                                                for (let children of this.mouseContainer.children)
                                                {
                                                    children.getComponent(sp.Skeleton).timeScale = 0;
                                                }
                                                this.showContinue();
                                            })
                                            .start();
                                    }).start();
                        }
                    })
                })
                .start();
            })
            .start();
    }

    runOption3():void {
        let isTrue = true;
        tween(this.lupin.node).delay(1)
        .call(() => {
            this.lupin.setAnimation(0, "emotion/excited", true);
            this.lupin.setAnimation(2, "emotion/excited", true);
            this.lupin.setAnimation(1, "level_6/throw_cheese", false);
            this.playSound(SOUND.THROW, false, 0.8)
            this.playSound(SOUND.MOUSE, false, 1.2)

            this.otherSpine[0].setMix("level_6/eat_cheese", "level_6/run_haypy", 0.3);
            tween(this.otherSpine[0]).delay(0.8).call(() => {
                this.otherSpine[0].setAnimation(1, "level_6/eat_cheese", false);
                this.otherSpine[0].setCompleteListener(track => {
                    if (track.animation.name == "level_6/eat_cheese" && isTrue)
                    {
                        isTrue = false;
                        this.otherSpine[0].setAnimation(1, "level_6/run_haypy", true);
                        this.otherSpine[0].node.scaleX = -0.8;
                        tween(this.otherSpine[0].node).by(2, {position: cc.v3(500, 0)})
                            .call(() => {
                                cc.audioEngine.stopAllEffects()
                                this.lupin.clearTrack(0);
                                this.lupin.clearTrack(2);
                                this.lupin.setAnimation(0, "emotion/sinister", false);
                                this.lupin.setAnimation(1, "general/crawl", true);
                                tween(this.lupin.node).delay(0.2)
                                    .call(() => {
                                        this.showSuccess(this.selected);
                                    })
                                    .by(1.2, {position: cc.v3(250, 0)})
                                    .start();
                            }).start();
                    }
                })
            })
            .start();
        })
        .start();
    }
}

