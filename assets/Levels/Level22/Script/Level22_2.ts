import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator
const tween = cc.tween;

@ccclass
export default class Level22_2 extends LevelBase {

    initStage(): void {
        super.initStage()
        this.setStatus()
        this.setAction()
    }

    setStatus(): void {
        this.setLupin(cc.v2(-919, -285), 'general/walk', 'emotion/happy_1')
        this.lupin.setCompleteListener(null)
        this.lupin.node.scale = 1
        this.lupin.timeScale = 1

        this.background.position = cc.v3(542, 0)

        this.otherSpine[0].setAnimation(0, 'chim_bay', true)
        this.otherSpine[0].node.position = cc.v3(-44, -21)
        this.otherSpine[0].node.active = false

        this.otherSprite[0].node.position = cc.v3(305, 617)
        this.otherSprite[0].node.angle = 0

        this.otherSprite[1].node.position = cc.v3(799, 413)
        this.otherSprite[1].node.angle = 0

        this.otherSprite[2].node.active = false
    }

    setAction(): void {
        cc.Tween.stopAllByTag(201)

        const shakeTween = tween().to(.1, {angle: .5}).to(.2, {angle: -.5})
        shakeTween.clone(this.otherSprite[0].node).tag(201).repeatForever().start()
        shakeTween.clone(this.otherSprite[1].node).tag(201).repeatForever().start()

        tween(this.lupin.node)
            .to(3, {position: cc.v3(-239, -285)})
            .call(() => {
                this.setLupin(
                    cc.v2(this.lupin.node.position),
                    'general/stand_thinking',
                    'emotion/thinking',
                )
            })
            .start()

        tween(this.background)
            .to(3, {position: cc.v3(-74, 0)})
            .to(1, {position: cc.v3(-534, 0)}, {easing: 'cubicOut'})
            .delay(2)
            .to(1, {position: cc.v3(-74, 0)}, {easing: 'cubicIn'})
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setAnimation(1, 'emotion/idle', true)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_22_2/mc_woodpecker') {
                this.setLupin(
                    cc.v2(this.lupin.node.position),
                    'general/stand_nervous',
                    'emotion/excited',
                )

                this.otherSpine[0].node.active = true

                tween(this.otherSpine[0].node)
                    .delay(1)
                    .to(3, {position: cc.v3(472, 542)})
                    .call(() => {
                        this.otherSpine[0].setAnimation(0, 'chim_go_kien', true)
                    })
                    .delay(2)
                    .call(() => {
                        cc.Tween.stopAllByTag(201)
                        this.otherSpine[0].setAnimation(0, 'chim_bay', true)

                        tween(this.otherSprite[0].node)
                            .to(.2, {angle: -42, position: cc.v3(327, 435)})
                            .to(.3, {angle: -62, position: cc.v3(367, 135)})
                            .start()

                        tween(this.otherSprite[1].node)
                            .to(.2, {angle: 72, position: cc.v3(686, 217)})
                            .call(() => {
                                this.lupin.setAnimation(0, 'general/win_1.1', true)
                            })
                            .to(.3, {angle: 95, position: cc.v3(730, -54)})
                            .call(() => {
                                this.onPass()
                            })
                            .start()
                    })
                    .start()
            }
        })


        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_22_2/mc_woodpecker', false)
        })
    }

    runOption2(): void {
        this.lupin.setAnimation(1, 'emotion/idle', true)
        this.lupin.clearTracks()

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_22_2/mc_throw_rope') {
                cc.Tween.stopAllByTag(201)
                this.showContinue()
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_22_2/mc_throw_rope', false)
        }, 1)
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_1/concrete_drilling') {
                this.lupin.setAnimation(0, 'level_1/concrete_drilling2', false)
            }

            if (track.animation.name === 'level_1/concrete_drilling2') {
                this.lupin.setCompleteListener(null)

                EffectManager.hideScene((node) => {
                    this.otherSprite[2].node.active = true
                    this.lupin.setAnimation(0, 'level_1/concrete_drilling', false)
                    this.lupin.node.scale = .1
                    this.lupin.timeScale = 0
                    this.lupin.node.position = cc.v3(61, 1160)

                    EffectManager.showScene()

                    tween(this.lupin.node)
                        .to(4, {position: cc.v3(61, -488)})
                        .call(() => {
                            this.showContinue()
                        })
                        .start()
                }, this.node)
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_1/concrete_drilling', false)
            this.lupin.setAnimation(1, 'emotion/sinister', true)
        }, 1)
    }
}
